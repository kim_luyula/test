<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('web.index');
    //echo "this is warmup only";
    //return view('welcome');
});

//return the getAbout function in pagesController
//'as' is for naming the route
Route::get('about', [
    'as' => 'about',
    'uses' => 'pagesController@getAbout'
]);

//return the getLogin function in pagesController
Route::get('login', [
    'as' => 'login',
    'uses' => 'pagesController@getLogin'
]);

//routing of the test page
Route::get('test', [
    'as' => 'test',
    'uses' => 'pagesController@getTest'
]);

//routing of the test2 page
//also get the data from the Product model
Route::get('test2', ['as' => 'test2', function(){
    $data = App\Product::all();
    return view('pages.test2') -> withData($data);
}]);

//Routing for edit button in test2
Route::post ( '/editItem', function (Request $request) {

    $rules = array (
        'name' => 'required|alpha',
        'price' => 'required'
    );
    $validator = Validator::make ( Input::all (), $rules );
    if ($validator->fails ())
        return Response::json ( array (
            'errors' => $validator->getMessageBag ()->toArray ()
        ) );
    else {

        $data = Data::find ( $request->id );
        $data->name = ($request->name);
        $data->price = ($request->price);
        $data->save ();
        return response ()->json ( $data );
    }
} );

//routing of the signin/signup view
Route::get ( 'customAuth', function () {
    return view ( 'customAuthentication' );
} );
Route::post ( '/login', 'MainController@login' );
Route::post ( '/register', 'MainController@register' );
Route::get ( '/logout', 'MainController@logout' );

//routing of the product view
Route::resource('product', 'productController');

//routing of the fletching of data to DataTables
Route::get('/serverSide', [
    'as'   => 'serverSide',
    'uses' => function () {
        $users = App\Product::all(); //

//        return $users;
        return Datatables::of($users)->make();
    }
]);