<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class customAuthController extends Controller
{
    public function register(Request $request) {
        $user = new User ();
        $user->name = $request->get ( 'username' );
        $user->email = $request->get ( 'email' );
        $user->password = Hash::make ( $request->get ( 'password' ) );
        $user->remember_token = $request->get ( '_token' );
        $user->save ();
        return redirect ( 'customAuth' );
    }

    public function login(Request $request) {
        if (Auth::attempt ( array (
            'name' => $request->get ( 'username' ),
            'password' => $request->get ( 'password' )
        ) )) {
            session ( [
                'name' => $request->get ( 'username' )
            ] );
            return Redirect::back ();
        } else {
            Session::flash ( 'message', "Invalid Credentials , Please try again." );
            return Redirect::back ();
        }
    }

    public function logout() {
        Session::flush ();
        Auth::logout ();
        return Redirect::back ();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
