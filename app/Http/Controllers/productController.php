<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\DomCrawler\Form;

class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all(); //all() is the eloquent function to retrieve all the data in Product
       // return $products;


//        return $products;

       return view("products.index") -> with("products", $products); //return the view of the database

        //return 'This is the index page of product'; //testing purpose

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("products.create"); //return the view of creating of products
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this -> validate($request, [
            'name' => 'required',
            'price' => 'required'
        ]); //validation process

        $product = new Product; //Product is the model
        $product->name = $request->name;
        $product->price = $request->price;
        $product->save(); //to save the product to the database

        //return "Something"; //for testing purpose

        //return redirect()->route('product.index'); // one way to redirect to product.index page

        return redirect()->action('productController@index');//another way to redirect using action
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //return $id; //testing only

        $product = Product::find($id); //retrieve a single product with that id

        //return $product;  //return the data

        return view("products.show")->with('products', $product); //return the view of show

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        //$product = Product::where('id', $id) -> first(); //another way to retrieve a data with that id

       // return $product;

        return view('products.edit', compact('product', $product)); //return the edit view of the product
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this -> validate($request, [
            'name' => 'required',
            'price' => 'required'
        ]); //validation process

        $product = Product::find($id);

        $product->name = $request->name;
        $product->price = $request->price;

        $product->save();

        return redirect() -> route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Product::destroy($id); //destroy method

        $product = Product::find($id)
            ->delete(); //delete method, another way to do the same thing

        return redirect()->route('product.index');
    }
}
