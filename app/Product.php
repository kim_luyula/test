<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;

    protected $table = 'products'; // products is the name of the table in database

    protected $fillable = ['name'];
}
