@extends('layout.layout')

@section('title')
    Edit product: {{$product->name}}
@stop

@section('content')

    {!! Form::model ($product,
        [
        'method'=>'patch',
        'route'=>['product.update', $product->id]
        ]
        ) !!}

    {!! Form::label('name', 'Name') !!}
    {!! Form::text('name', $product->name) !!}

    <br>

    {!! Form::label('price', 'Price') !!}
    {!! Form::text('price', $product->price) !!}

    {!! Form::submit('Edit') !!}

    {!! Form::close() !!}

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

@stop