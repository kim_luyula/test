@extends('layout.layout')

@section('title')
    All Products
@stop

@section('header')
    <link
            href="https://cdn.datatables.net/1.10.12/css/dataTables.material.min.css"
            rel="stylesheet">
    <link
            href="//cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css"
            rel="stylesheet">

    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <style>
        tfoot input {
            width: 100%;
            padding: 3px;
            box-sizing: border-box;
        }
    </style>
@stop


@section('content')

    <table id="example" class="datatable mdl-data-table dataTable" cellspacing="0"
           width="100%" role="grid"
           style="width: 100%;">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Product</th>
        </tr>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Product</th>
        </tr>
        </tfoot>
        </thead>
        <tbody>
        </tbody>
    </table>

    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.material.min.js"></script>

    <script src="//code.jquery.com/jquery-1.12.3.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>


    <script>
/*        //initialize the data table
        $(document).ready(function(){

            $('#example').DataTable({
                processing: true,
                serverSide: true, //it calls route to serverSide, so check if the route is created in routes.php
                ajax: '{{ route('serverSide') }}',
                columnDefs: [
                    {
                        targets: [ 0, 1, 2 ],
                        className: 'mdl-data-table__cell--non-numeric'
                    }
                ]
            });
        });*/

        //column searching by text
        $(document).ready(function() {
            // Setup - add a text input to each footer cell, careful if there is footer in the table
            $('#example tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );
            // initialize the data table from serverSide
            var table = $('#example').DataTable({
                processing: true,
                serverSide: true, //it calls route to serverSide, so check if the route is created in routes.php
                ajax: '{{ route('serverSide') }}',
                    });
            // Apply the search
            table.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                                .search( this.value )
                                .draw();
                    }
                } );
            } );
        } );
    </script>

   <h1>testing for translation: {{trans('main.main')}}</h1>

   <h1><a href="{{route('product.create')}}">Create a new product</a></h1>

    @foreach($products as $product)

        <h1>no. {{$product -> id}}</h1>
        <h1>Name: {{$product -> name}}</h1>
        <h1>Price: {{$product -> price}}</h1>

        <h1><a href="{{route('product.show', $product->id)}}">Check this product</a></h1>

        <br>

    @endforeach



@stop