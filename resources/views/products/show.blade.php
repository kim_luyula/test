@extends('layout.layout')

@section('title')
    {{$products->name}}
@stop

@section('content')

    {!!Form::open(
        [
        'method'=>'delete',
        'route'=>['product.destroy', $products->id]
        ]
        )!!}

    <h1>Name: {{$products->name}}</h1>
    <h1>Price: {{$products->price}}</h1>

    <a href="{{route('product.edit', $products->id)}}">Edit</a>

    <br>

    {!!Form::submit('Delete')!!}

    {!!Form::close()!!}

@stop