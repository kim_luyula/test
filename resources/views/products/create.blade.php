@extends('layout.layout')

@section('title')
    Create new product
@stop

@section('content')

    <h1>Create a new product</h1>

{!! Form::open (['route'=>'product.store']) !!}

{!! Form::label('name', 'Name') !!}
{!! Form::text('name') !!}
{!! Form::label('price', 'Price') !!}
{!! Form::text('price') !!}

{!! Form::submit('Create') !!}

{!! Form::close() !!}

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


@stop