@extends('layout.layout')

@section('title')
    Test2
@stop

@section('header')

    <script src="//code.jquery.com/jquery-1.12.3.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script
            src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script
            src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet"
          href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">

    <style>
    </style>
@stop

@section('content')

    <h1>Test2: DataTable non server side</h1>

    <table class="table" id="table">
        <thead>
        <tr>
            <th class="text-center">id</th>
            <th class="text-center">Name</th>
            <th class="text-center">Product</th>
            <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr class="item{{$item->id}}">
                <td>{{$item->id}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->price}}</td>
                <td><button class="edit-modal btn btn-info"
                            data-info="{{$item->id}},{{$item->name}},{{$item->price}}">
                        <span class="glyphicon glyphicon-edit"></span> Edit
                    </button>
                    <button class="delete-modal btn btn-danger"
                            data-info="{{$item->id}},{{$item->name}},{{$item->price}}">
                        <span class="glyphicon glyphicon-trash"></span> Delete
                    </button></td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div id="myModal" class="modal fade" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Edit</h4>

                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id">ID</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fid" disabled="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="name">Name</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" id="name">
                            </div>
                        </div>
                        <p class="name_error error text-center alert alert-danger hidden"></p>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="price">Price:</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" id="price">
                            </div>
                        </div>
                        <p class="price_error error text-center alert alert-danger hidden"></p>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="gender">Gender</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="gender" name="gender">
                                    <option value="" disabled="" selected="">Choose your option</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>

                    </form>
                    <div class="deleteContent" style="display: none;">
                        Are you Sure you want to delete <span class="dname"></span> ? <span class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn actionBtn btn-success edit" data-dismiss="modal">
                            <span id="footer_action_button" class="glyphicon glyphicon-check"> Update</span>
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remove"></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>

        

    <script>
        //init the datatable
        $(document).ready(function() {
            $('#table').DataTable();
        } );

        //the modal after the edit button is clicked, it has an update button
        $(document).on('click', '.edit-modal', function() {
            $('#footer_action_button').text(" Update");
            $('#footer_action_button').addClass('glyphicon-check');
            $('#footer_action_button').removeClass('glyphicon-trash');
            $('.actionBtn').addClass('btn-success');
            $('.actionBtn').removeClass('btn-danger');
            $('.actionBtn').removeClass('delete');
            $('.actionBtn').addClass('edit');
            $('.modal-title').text('Edit');
            $('.deleteContent').hide();
            $('.form-horizontal').show();
            var stuff = $(this).data('info').split(',');
            fillmodalData(stuff)
            $('#myModal').modal('show');
        });

        //a function to get the data for editing
        function fillmodalData(details){
            $('#id').val(details[0]);
            $('#name').val(details[1]);
            $('#price').val(details[2]);
        }

        //the logic for update
        $('.modal-footer').on('click', '.edit', function() {
            $.ajax({
                type: 'post',
                url: '/editItem',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $("#id").val(),
                    'name': $('#name').val(),
                    'price': $('#price').val(),

                },
                success: function(data) {
                    if (data.errors){
                        $('#myModal').modal('show');
                        if(data.errors.name) {
                            $('.name_error').removeClass('hidden');
                            $('.name_error').text("Name can't be empty !");
                        }
                        if(data.errors.price) {
                            $('.price_error').removeClass('hidden');
                            $('.price_error').text("Price can't be empty !");
                        }
                    }
                    else {

                        $('.error').addClass('hidden');
                        $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" +
                                data.id + "</td><td>" + data.name +
                                "</td><td>" + data.price +
                                "</td><td><button class='edit-modal btn btn-info' data-info='" + data.id+","+data.name+","+data.price+"'>" +
                                "<span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-info='" + data.id+","+data.name+","+data.price+"' ><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");

                    }}
            });
        });


    </script>


@stop