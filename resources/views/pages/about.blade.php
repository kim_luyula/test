@extends('layout.layout')

@section('title')
    About page
@stop

@section('content')

    <h2>{{$companyName}}</h2>

    @if($isRegistered == true)
        <h1>Welcome</h1>
    @else
        <h3>Please register!</h3>
    @endif

    @for ($i = 0; $i < 10; $i++)
        <p>The current value is {{ $i }}</p>
    @endfor

@stop