@extends('layout.layout')

@section('title')
    Login page
@stop

@section('content')

    <h1>Please login here</h1>


    {!! Form::label('username', 'Username') !!}
    {!! Form::text('username') !!}
    {!! Form::label('password', 'Password') !!}
    {!! Form::text('password') !!}

    {!! Form::submit('Click here to login') !!}


@stop