<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    @yield('header')

</head>
<body>

    @include('layout.menu')

    @yield('content')

</body>
</html>